# -*- coding: utf-8 -*-
import scrapy
import re
import googlemaps
from datetime import datetime

gmaps = googlemaps.Client(key='AIzaSyBcVRE_ltD55OEPsvrTP8kVJ5qAfcpTfeA')


class FundaCrawlerSpider(scrapy.Spider):
    name = 'funda_sold'
    allowed_domains = ['funda.nl']
    start_urls = ['https://www.funda.nl/en/koop/amsterdam/verkocht/100000+/+5km/sorteer-afmelddatum-af/']
    max_pages_to_parse = 500
    count = 1

    def parse(self, response):
        ads = response.css('.search-result')

        # limitation for temp purposes
        # urls = urls[:3]

        for ad in ads:
            price = ad.css('.search-result-price::text').extract_first().strip()
            if price != 'Price on request':
                url = response.urljoin(ad.css('.search-result-header a::attr(href)').extract_first())
                yield scrapy.Request(url=url, callback=self.parse_details)

        # follow pagination link
        next_page_link = response.css('.pagination a[rel="next"]::attr(href)').extract_first()
        if next_page_link and (self.count < self.max_pages_to_parse):
            next_page_link = response.urljoin(next_page_link)
            self.count += 1
            self.log('Following to the page ' + str(self.count))
            yield scrapy.Request(url=next_page_link, callback=self.parse)

    def parse_details(self, response):
        header = response.css('.object-header__content')
        features = response.css('.object-kenmerken.is-expanded')
        address_selector = header.css('.object-header__address')

        funda_id = int(address_selector.css('::attr(data-global-id)').extract_first())

        zipcode_with_city = address_selector.css('.object-header__address-city::text').extract_first()
        zipcode_with_city_parsed = re.search('(\d+\s\w+)\s(\w+)', zipcode_with_city)
        if not zipcode_with_city_parsed:
            return

        zipcode = zipcode_with_city_parsed.group(1)
        city = zipcode_with_city_parsed.group(2)

        address = address_selector.css('::text').extract_first().strip()
        geocode_result = gmaps.geocode(' '.join([address, city, zipcode]))
        if not len(geocode_result):
            return

        coordinates = geocode_result[0]['geometry']['location']
        latitude = coordinates['lat']
        longitude = coordinates['lng']

        price_with_currency = header.css('.object-header__price--historic::text').extract_first()
        price_with_currency = re.sub('[,\.]', '', price_with_currency)
        price = int(re.search('\d+', price_with_currency).group(0))

        listed_since = response.css('dt:contains("Listed since") + dd::text').extract_first()
        listed_since_iso = None
        if listed_since:
            listed_since_iso = datetime.strptime(listed_since, '%B %d, %Y').isoformat()

        date_of_sale = response.css('dt:contains("Date of sale") + dd::text').extract_first()
        date_of_sale_iso = None
        if date_of_sale:
            date_of_sale_iso = datetime.strptime(date_of_sale, '%B %d, %Y').isoformat()

        service_charges = features.css('dt:contains("Service charges") + dd::text').extract_first()
        vve_contribution = features.css('dt:contains("VVE (Owners Association)") + dd::text').extract_first()
        service_cost = service_charges or vve_contribution
        if service_cost:
            service_cost = int(re.search('\d+', service_cost).group(0))
        else:
            service_cost = 0

        type_apartment = features.css('dt:contains("Type apartment") + dd::text').extract_first()
        kind_of_house = features.css('dt:contains("Kind of house") + dd::text').extract_first()
        estate_type = None
        if type_apartment or kind_of_house:
            estate_type = (type_apartment or kind_of_house).strip()

        building_type = features.css('dt:contains("Building type") + dd::text').extract_first()
        if building_type:
            building_type = building_type.strip()

        year_of_construction = features.css('dt:contains("Year of construction") + dd::text').extract_first()
        if year_of_construction:
            if re.match('Before \d+', year_of_construction):
                year_of_construction = re.search('\d+', year_of_construction).group(0)
            if re.match('After \d+', year_of_construction):
                year_of_construction = re.search('\d+', year_of_construction).group(0)

        construction_period = features.css('dt:contains("Construction period") + dd::text').extract_first()
        if construction_period and not year_of_construction:
            construction_period = construction_period.strip()
            if re.match('^Before \d+$', construction_period):
                year_of_construction = re.search('\d+', construction_period).group(0)
            else:
                year_of_construction = re.search('(\d+)-(\d+)', construction_period).group(2)

        if year_of_construction:
            year_of_construction = int(year_of_construction.strip())

        living_area = features.css('dt:contains("Living area") + dd::text').extract_first()
        area = features.css('dt:contains("Area") + dd::text').extract_first()
        living_area_value = int(re.search('\d+', living_area or area).group(0))
        price_per_meter = price / living_area_value
        volume = features.css('dt:contains("Volume in cubic meters") + dd::text').extract_first()
        volume_value = int(re.search('\d+', volume).group(0))

        exterior_space = features.css('dt:contains("Exterior space attached") + dd::text').extract_first()
        if exterior_space:
            exterior_space = int(re.search('\d+', exterior_space).group(0))
        else:
            exterior_space = 0

        external_storage = features.css('dt:contains("External storage space") + dd::text').extract_first()
        if external_storage:
            external_storage = int(re.search('\d+', external_storage).group(0))
        else:
            external_storage = 0

        rooms = features.css('dt:contains("Number of rooms") + dd::text').extract_first().strip()
        if re.match('^\d+ room(s)?$', rooms) :
            number_of_rooms = re.search('\d+', rooms)
            number_of_rooms_value = int(number_of_rooms.group(0))
            number_of_bedrooms_value = 0
        else:
            number_of_rooms = re.search('(\d+).+(\d+)', rooms)
            number_of_rooms_value = int(number_of_rooms.group(1))
            number_of_bedrooms_value = int(number_of_rooms.group(2))

        energy_label = features.css('dt:contains("Energy label") + dd span.energielabel::text').extract_first()
        if energy_label:
            energy_label = energy_label.strip()

        insulation = features.css('dt:contains("Insulation") + dd::text').extract_first()
        if insulation:
            insulation = insulation.strip()

        terrace = bool(features.css('dt:contains("Balcony/roof garden") + dd:contains("Roof terrace")::text').extract_first())
        garden = bool(features.css('dt:contains("garden") + dd::text').extract_first())

        yield {
            'funda_id': funda_id,
            'address': address,
            'latitude': latitude,
            'longitude': longitude,
            'zipcode': zipcode,
            'city': city,
            'price': price,
            'price_per_meter': price_per_meter,
            'service_cost': service_cost,
            'estate_type': estate_type,
            'building_type': building_type,
            'year_of_construction': year_of_construction,
            'living_area': living_area_value,
            'exterior_space': exterior_space,
            'external_storage': external_storage,
            'volume': volume_value,
            'number_of_rooms': number_of_rooms_value,
            'number_of_bedrooms': number_of_bedrooms_value,
            'energy_label': energy_label,
            'insulation': insulation,
            'terrace': terrace,
            'garden': garden,
            'url': response.request.url,
            'listed_since': listed_since_iso,
            'date_of_sale': date_of_sale_iso
        }

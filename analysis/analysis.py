import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split

df = pd.read_csv('../out.csv', parse_dates=True, index_col='date_of_sale')
df.sort_index(inplace=True)

# exclude everything with a price above or below 3 standard deviations (i.e. outliers)
df = df[np.abs(df['price_per_meter'] - df['price_per_meter'].mean()) <= (3 * df['price_per_meter'].std())]

# Histogram - Asking Price per m2 (EUR)
# plt.hist(df['price_per_meter'], bins=50, normed=False, histtype='stepfilled', alpha=0.7)
# plt.title('Histogram - Asking price (EUR)')
# plt.show()

# Correlation matrix
# corr = df.drop('zipcode', axis = 1).corr()
# print(corr)

plot_df = pd.DataFrame()
plot_df['price_per_meter'] = df['price_per_meter'].resample('1D').mean()
plot_df['avg_30'] = plot_df['price_per_meter'].rolling(30, min_periods=1).mean()
plot_df['avg_50'] = plot_df['price_per_meter'].rolling(50, min_periods=1).mean()

avg_30 = plot_df['avg_30'].plot(label='avg_30')
avg_50 = plot_df['avg_50'].plot(label='avg_50')

# plt.style.use('seaborn')
plt.legend()
plt.show()

# data = df.drop(columns=['funda_id', 'address', 'price', 'zipcode', 'city', 'estate_type', 'building_type', 'insulation', 'url', 'listed_since', 'date_of_sale'])

# categories labeling
# data['energy_label'] = data['energy_label'].astype('category').cat.codes

# normalizing
# data = (data - data.mean()) / (data.std())

# y = data.price_per_meter
# x = data.drop(columns=['price_per_meter'])

# x_train, x_test, y_train, y_test = train_test_split(x, y, random_state=0)
